﻿using Tesseract;
using Tesseract.Droid;
using TesseractDemo.Droid.Services;
using TesseractDemo.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(ServiceHelper))]
namespace TesseractDemo.Droid.Services
{
    class ServiceHelper : IServiceHelper
    {
        public ITesseractApi GetTesseractApi()
        {
            var a = Android.App.Application.Context;
            var b = new TesseractApi(a, AssetsDeployment.OncePerInitialization);
            return b;
        }
    }
}