﻿using Tesseract;
using Tesseract.iOS;
using TesseractDemo.iOS.Services;
using TesseractDemo.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(ServiceHelper))]
namespace TesseractDemo.iOS.Services
{
    public class ServiceHelper : IServiceHelper
    {
        public ITesseractApi GetTesseractApi()
    {
            return new TesseractApi();
        }
}
}
