﻿using System;
using System.Collections.Generic;
using System.Text;
using Tesseract;

namespace TesseractDemo.Services
{
    public interface IServiceHelper
    {
        ITesseractApi GetTesseractApi();
    }
}
