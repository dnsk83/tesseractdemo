﻿using Plugin.Media;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Input;
using Tesseract;
using TesseractDemo.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace TesseractDemo.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private const int MaximumImageSideLength = 1280; // Optimal value found empirically

        private string _fileName;
        private ImageSource _image;
        private string _ocrResult;
        private int _tesseractProgress;
        private bool _processing;

        public ICommand RecognizePhotoCommand { get; private set; }
        public string FileName
        {
            get => _fileName;
            private set
            {
                _fileName = value;
                OnPropertyChanged();
            }
        }
        public ImageSource Image 
        {
            get => _image;
            private set
            {
                _image = value;
                OnPropertyChanged();
            }
        }
        public string OcrResult 
        {
            get => _ocrResult;
            private set
            {
                _ocrResult = value;
                OnPropertyChanged();
            }
        }
        public int TesseractProgress
        {
            get => _tesseractProgress;
            set
            {
                _tesseractProgress = value;
                OnPropertyChanged();
            }
        }
        public bool Processing
        {
            get => _processing;
            private set
            {
                _processing = value;
                OnPropertyChanged();
            }
        }

        public MainViewModel()
        {
            InitCommands();
        }

        private void InitCommands()
        {
            RecognizePhotoCommand = new Command(RecognizePhotoAsync);
        }

        private async void RecognizePhotoAsync(object obj)
        {
            await CrossMedia.Current.Initialize();

            var photo = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions()
            {
                Directory = "TesseractDemo",
                Name = "photo.jpg",
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.MaxWidthHeight,
                MaxWidthHeight = MaximumImageSideLength
            });

            if (photo == null) return;

            Image = ImageSource.FromFile(photo.Path);

            byte[] imageArray = null;

            using (MemoryStream memory = new MemoryStream())
            {
                Stream stream = photo.GetStream();
                stream.CopyTo(memory);
                imageArray = memory.ToArray();
            }

            OcrResult = string.Empty;

            RecognizeAsync(imageArray);
        }

        private async void RecognizeAsync(byte[] imageArray)
        {
            Processing = true;
            var tesseractApi = DependencyService.Get<IServiceHelper>().GetTesseractApi();
            var initialized = await tesseractApi.Init("eng+rus");
            if (!initialized) return;
            try
            {
                tesseractApi.Progress += (s, e) =>
                {
                    TesseractProgress = e.Progress;
                };
                var success = await tesseractApi.SetImage(imageArray);
                if (success)
                {
                    var result = tesseractApi.Text;
                    OcrResult = result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Processing = false;
            }
        }
    }
}
